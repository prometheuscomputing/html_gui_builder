require_relative 'abstract_collection_controller'

module Gui::WidgetControllers
  class CollectionController < AbstractCollectionController
    def self.default_locals
      super.merge({
        :hide_collection_header => false
      })
    end
  end
end
