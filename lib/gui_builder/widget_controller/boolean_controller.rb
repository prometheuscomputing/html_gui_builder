require_relative 'choice_controller'

module Gui::WidgetControllers
  class BooleanController < WidgetController
    def initial_field_value
      ret = initial_value['value'] || domain_obj
      ret = "false" if ret.class == FalseClass
      ret ||= ''
      ret
    end
    
    # The value to be displayed for the boolean
    # This is not the same as the 'value' of the boolean, like it would be for a 'choice' widget
    def boolean_display_value(value, widget_choices)
      case value
      when true
        widget_choices[0]
      when false
        widget_choices[1]
      when nil
        '-'
      end
    end
  end
end