require_relative 'collection_controller'

module Gui::WidgetControllers
  # This is a special case when an organizer (from the spec) needs to morph into
  # a collection.  This will be done when a to_one association does not exist
  # and we want a nice table to show possible associations to create.  Since it
  # is a to_one association, the default view is an Organizer instead of a
  # Collection.
  class CollectionFromOrganizerController < CollectionController

    # @override
    def domain_obj
      # likely domain_obj is not Enumerable, so we'll wrap it in an array.  But
      # it is also likely that it is a 'new' domain object that hasn't been
      # created/saved to the database yet.  We only want to show domain objects
      # that exist in the database, so we filter that out.
      # - Filtering on exists is taking a very long time on large datasets.
      result_from_super = super
      if result_from_super.nil?
        ret = []
      else
        ret = (result_from_super.kind_of?(Enumerable) ? result_from_super : [result_from_super])
      end

      return ret
    end

    # @override
    def view_name
      widget.view_name
    end
  end
end