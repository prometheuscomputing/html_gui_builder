require_relative 'widget_controller'

module Gui::WidgetControllers
  class ChoiceController < WidgetController
  
    def field_value
      fv = previous_value['value'] || domain_obj
      interpret_enumeration_literal_values(fv)
    end

    def initial_field_value
      ifv = initial_value['value'] || domain_obj || ''
      interpret_enumeration_literal_values(ifv)
    end
    
    def enumeration_choices parent_obj, literals_getter, legacy_choices
      # return [] unless parent_obj
      if parent_obj.respond_to? literals_getter
        parent_obj.send(literals_getter)
      else
        legacy_choices
      end
    end
   
    def interpret_enumeration_literal_values(literal_objects)
      strings = Array(literal_objects).collect do |o|
        if o.is_a?(TrueClass) || o.is_a?(FalseClass)
          o.to_s
        elsif o.is_a? String
          o
        else
          o.value
        end
      end
      return strings.first if strings.count < 2
      strings
    end
  end
end