require_relative 'base_controller'

# In the interest of making it clear that we have MVC going on...
# Here is the controller.  The *model* is an instance of Gui::Widgets and is @vertex.widget.  The view...well, it isn't the things we're getting from vertex.view, because that thing returns a widget model.  So, what is the view?

module Gui::WidgetControllers
  class WidgetController < BaseController
    HIDE_CLASS = 'hide'
    attr_accessor :vertex
    def initialize(args = {})
      super
      @vertex = args[:vertex]
    end
    
    def current_user
      vertex.current_user
    end
    
    def widget
      @widget ||= vertex&.widget
    end
        
    def view
      @view ||= vertex&.view
    end
    
    # TODO domain_obj is a bad name for what this is.  domain_obj implies that it is an object from the application domain but in reality this could be some other type of object, e.g. an AdHocPageObject
    def domain_obj
      @domain_obj ||= vertex.domain_obj
    end
    
    def through_obj
      @through_obj ||= vertex.through_obj
    end
    
    def parent_obj
      @parent_obj ||= vertex.get_parent_obj
    end

    def parent_classifier
      @parent_classifier ||= vertex.parent_classifier
    end
    
    def parent_id
      @parent_id ||= vertex.parent_id
    end
    
    def data_classifier
      @data_classifier ||= vertex.domain_classifier
    end
    
    def through_classifier 
      # return @through_classifier if @through_classifier
      # classifier = widget.through_classifier
      @through_classifier ||= widget.through_classifier ? widget.through_classifier : vertex.through_classifier
      # @through_classifier = vertex.through_classifier
    end

    def data_id
      # wasTODO: use vertex definition instead, once Generated Site is fixed
      # generated site is more or less defunct so we are going to use vertex's definition
      @data_id ||= vertex.domain_id
      # (domain_obj && domain_obj.is_a?(ORM_Class)) ? (domain_obj.id || 'new') : nil
    end
    
    def parent_vertex
      @parent_vertex ||= vertex.parent
      # (domain_obj && domain_obj.is_a?(ORM_Class)) ? (domain_obj.id || 'new') : nil
    end
    
    def do_root_load_hooks
      domain_obj.on_gui_root_load if domain_obj.respond_to?(:on_gui_root_load)
    end        
    
    def domain_obj_count
      vertex.domain_obj_count
    end
        
    def field_value
      previous_value['value'] || (@association_class_widget ? through_obj : domain_obj)
    end

    def initial_field_value
      initial_value['value'] || (@association_class_widget ? through_obj : domain_obj) || ''
      # if @association_class_widget
      #   initial_value['value'] || through_obj || ''
      # else
      #   initial_value['value'] || domain_obj || ''
      # end
    end
    
    def previous_value
      locals[:continue_edit] ? vertex.previous_object_state || {} : {}
    end
    
    def initial_value
      locals[:continue_edit] ? vertex.initial_object_state || {} : {}
    end
    
    def last_update
      domain_obj[:updated_at] # column access because it is added by the timestamps plugin, which doesn't add the method.
    end

    def association_info
      return @assoc_info if @assoc_info
      if widget.getter && parent_classifier && parent_classifier.respond_to?(:associations)
        #Use the parents through classifier or the derived through classifier for nested widgets
        #TODO make this into a controller method since it is used in is composer as well
        if widget.association_class_widget
          klass = widget.derived_parent_through_classifier || widget.parent.through_classifier
        else
          klass = parent_classifier
        end
        assoc_info = {}
        getter_chain = widget.getter.split('.')
        @assoc_info = getter_chain.reduce({}) do |info, getter|
          info = klass.properties[getter.to_sym]
          return nil unless info # never able to cache if nil
          # raise "Couldn't get assoc_info for #{getter.to_sym.inspect} association on #{klass}" unless info
          klass = Gui.resolve_classifier(info[:class])
          # klass = Gui.resolve_classifier(info[:class]) unless widget.association_class_widget
          info
        end
      else
        @assoc_info = {}
      end
    end
    
    def is_composition?
      return false unless parent_vertex
      raise "Unable to find association information for #{widget.getter}" unless association_info
      association_info[:composition]
    end
    
    def is_composer?
      return @is_composer unless @is_composer.nil?
      if parent_vertex
        if widget.association_class_widget
          parent_class = widget.derived_parent_through_classifier || widget.parent.through_classifier
        else
          parent_class = parent_classifier
        end
        assoc_info = {}
        getter_chain = widget.getter.split('.')
        klass = getter_chain.reduce(parent_class) do |klass, getter|
          assoc_info = klass.associations[getter.to_sym]
          Gui.resolve_classifier(assoc_info[:class])
        end
        opp_assoc_info = klass.associations[assoc_info[:opp_getter]]
        # All normal associations will have opp_assoc_info defined, but we have to check if nil
        # since Gui::Home does not have an :opp_getter and opp_assoc_info will be nil
        @is_composer = opp_assoc_info ? !!opp_assoc_info[:composition] : false
      else
        @is_composer = false
      end
    end
    
    def assoc_class
      (join_class && join_class.association_class?) ? join_class : nil
    end
    alias_method :has_assoc_class?, :assoc_class
    
    def has_join_class?
      association_info[:through]
    end
    
    def join_class
      # return @join_class if @join_class
      # return nil unless association_info[:through]
      @join_class ||= association_info[:through]&.to_const
    end
    
    # Essentially a copy of the functionality of vertex#pretty_name
    # This is duplicated to allow template rendering
    # def pretty_name(parent_obj, getter)
    #   classifier = parent_obj ? parent_obj.class.to_s.gsub(':', '-') : ''
    #   identifier = parent_obj ? parent_obj.id : 'new'
    #   html_escape("#{classifier}__#{identifier}__#{getter}")
    # end
    
    def pretty_name
      return @pretty_name if @pretty_name      
      classifier = parent_obj ? parent_obj.class.to_s.gsub(':', '-') : ''
      identifier = parent_obj ? parent_obj.id : 'new'
      @pretty_name = html_escape("#{classifier}__#{identifier}__#{widget.getter}")
    end
    
    #TODO This really sucks.  There has to be a better way - TB
    def widget_comment_count
      0
      # if Gui.option(:enable_document_comments)
      #   widget_pretty_getter = pretty_name
      #   #This has to be run once per widget in which it is used I don't like it
      #   #TODO if this is ever allowed for widget comments on the advanced view this table will need to be adapated
      #   comment_count = DB[:document_widget_comments].select(:id).where(:widget_pretty_name => widget_pretty_getter).count
      # else
      #   0
      # end
    end
    
    def stringify_style(style)
      style.is_a?(Hash) ? style.map { |k,v| "#{k}: #{v};" }.join(' ') : style
    end
    
    # Sometimes we need to manipulate widgets based on factors not directly accessible to the widget.  Here we are passing the locals from the context in which this widget is being rendered.  Example, passing the locals from a collection page to buttons rendered on that collection page via ButtonWidget.test_conditionals.
    def test_conditionals(locals)
    end
    
  end
end
