require_relative 'widget_controller'

module Gui::WidgetControllers
  class MessageController < WidgetController
    def field_value
      val = super.to_s
      # Inserts an HTML line break before all newline characters.
      # Named after builtin PHP method with the same function.
      # val.gsub /\n/, "<br />\n"
      val
    end
  end
end