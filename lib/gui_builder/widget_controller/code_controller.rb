require_relative 'text_controller'

module Gui::WidgetControllers
  class CodeController < TextController
    def domain_obj_language
      domain_obj.language if domain_obj
    end
    def selected_language
      (domain_obj_language || widget.language || widget.default_language_string)
    end
  end
end