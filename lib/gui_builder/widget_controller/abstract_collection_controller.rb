require_relative 'abstract_view_controller'

module Gui::WidgetControllers
  class AbstractCollectionController < AbstractViewController
    def self.default_locals
      super.merge({
        :unassociated_only => false,
        :selection         => false,
        :selection_create  => false
      })
    end
    
    def selection_param_name
      locals[:selection_param_name] || "select_#{vertex.pretty_id}[]"
    end
  end
end