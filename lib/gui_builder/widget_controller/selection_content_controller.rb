require_relative 'collection_content_controller'

module Gui::WidgetControllers
  # Is a type of CollectionContentController where the domain obj is a given collection of objects.
  # No widget or vertex is available, so most methods will be overridden with dummies
  class SelectionContentController < CollectionContentController    
    def association_info; {:getter => :conflicts, :type => :one_to_many}; end
    def title; 'Conflicts'; end
    def filter_type(widget); ''; end
    def domain_obj; @domain_obj; end
    def unassociated_obj; []; end
  end
end