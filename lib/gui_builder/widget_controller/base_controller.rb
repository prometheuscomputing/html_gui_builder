require 'method_source'

module Gui::WidgetControllers
  class BaseController
    include Gui::PolicyMethods
    attr_accessor :content, :locals, :controller_type, :display_type, :params_in_use
    attr_reader   :current_user
    @@widget_templates = {}
    def self.default_locals
      {}
    end
    
    def self.widget_templates
      return @@widget_templates unless @@widget_templates.empty?
      paths = Pathname(relative('../widget_template')).glob("**/*.haml")
      paths.each do |path|
        raise "Each widget template must have a unique name regardless of directory structure.  #{path.basename} is not unique!" if paths.find { |other_path| next if other_path == path; other_path.basename == path.basename }
        key = path.basename.sub('.haml', '')
        @@widget_templates[key.to_s] = path.to_s
      end
      @@widget_templates
    end

    def self.template_path(type = nil, display_type = nil)
      # type ||= name.split('::').last.sub(/Controller$/, '') # this is only here to support the commented out inheritence below
      
      # Hack to use collection_content template for selection_content's as well.
      type = "CollectionContent" if type == "SelectionContent"

      template_name = type.to_snakecase 
      template_name << '_' + display_type.to_s if display_type

      path = widget_templates[template_name]
      if !path && display_type 
        fall_back_path = widget_templates[type.to_snakecase] # fall back on base template if there is no template for the display_type
        path = fall_back_path if fall_back_path
      end

      # # inheritence for templates...more or less -- THIS IS NOT TESTED (and so far isn't being used...)
      # unless path
      #   ancestors.each do |ancestor|
      #     break unless ancestor.name =~ /Gui::WidgetControllers
      #     break if ancestor == Gui::WidgetControllers:WidgetController || ancestor == Gui::WidgetControllers:AbstractViewController
      #     next  if ancestor == self
      #     inherited_path = ancestor.template_path(ancestor.name.split('::').last.sub(/Controller$/, ''), display_type)
      #     if File.exists?(inherited_path)
      #       path = inherited_path
      #       break
      #     end
      #   end
      # end
      
      raise "Couldn't find template path: #{template_name}.haml for #{self.name}" unless path
      path
    end
    
    def self.get_haml_engine(type, display_type = nil)
      @@engines ||= {}
      #  reload templates if in :dev mode.  tests must be run in :live mode.
      if Gui.option(:mode) == :dev
        Haml::Engine.new(get_haml_template(type, display_type), :filename => template_path(type, display_type))
      else
        @@engines[[type, display_type]] ||= Haml::Engine.new(get_haml_template(type, display_type), :filename => template_path(type, display_type))
      end
    end
    
    def self.get_haml_template(type, display_type = nil)
      @@templates ||= {}
      #  reload templates if in :dev mode.  tests must be run in :live mode.
      if Gui.option(:mode) == :dev
        File.read(template_path(type, display_type))
      else
        @@templates[[type, display_type]] ||= File.read(template_path(type, display_type))
      end
    end
    
    def initialize(args = {})
      Gui.input_options(args[:locals])
      self.locals           = self.class.default_locals.merge(args[:locals])
      self.controller_type  = args[:controller_type]
      self.display_type     = args[:display_type]
      self.content          = nil
      @params_in_use        = {}
    end
    
    def render(&block)
      log_info "       About to Haml render"
      # TODO: Use cached engine, which will be found by type
      type = locals[:render_custom_templates] ? controller_type + '_' + locals[:render_custom_templates] : controller_type
      engine = self.class.get_haml_engine(type, display_type)
      begin
        output = engine.render(self, locals, &block)
      rescue => e
        puts "Rendering #{type} (#{self.class.template_path(type, display_type)}) caused error: #{e.message}"
        raise e
      end
      self.content = output
    end
    
    def domain_obj_string(domain_obj)
      case domain_obj
      when NilClass
        'nil'
      when Array
        domain_obj.map { |obj| domain_obj_string(obj) }
      when ORM_Class
        if domain_obj.id
          "#{domain_obj.class}[#{domain_obj.id}]"
        else
          "#{domain_obj.class}.new"
        end
      end
    end

    # Returns whether or not there is a reference to this variable/method in the given template
    def has_reference(template, var_name)
      # TODO: ignore comment lines
      !!(template =~ /(?<![\:\._%a-zA-Z])#{var_name}(?![_a-z])/)
    end
    
    # Replaces all references to a variable within the template with the given value
    def replace_references!(template, var_name, value = nil, evaluate = false)
      # TODO: ignore comment lines
      #!!(template =~ /(?<![\:\._%a-zA-Z])#{var_name}(?![_a-z])/)
      
      # Escape any question marks or brackets found in var/method name before using as regular expression
      escaped_name = var_name.gsub(/([\?\[\]])/, '\\\\\1')
      # Replace interpolated string variables
      template.gsub!(/#\{#{escaped_name}\}/){ eval(var_name).to_s } if evaluate
      # Replace normal variable reference
      # TODO: upgrade the below regex to exclude comment lines
      template.gsub!(/(?<![\:\._%a-zA-Z@])#{escaped_name}(?![_a-z])/){ evaluate ? eval(var_name).inspect : value }
    end
  end
end
