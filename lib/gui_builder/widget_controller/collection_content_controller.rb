require 'nokogiri'
require_relative 'abstract_collection_controller'

module Gui::WidgetControllers
  class CollectionContentController < AbstractCollectionController
    def self.default_locals
      super.merge({
        :page_index                 => 0,
        :show_possible_assoc        => false,
        :search_all_filter          => {},
        :simple_filter              => {},
        :show_filter_row            => Gui.option(:advanced_search_open),
        :enable_search_all_filter   => Gui.option(:search_all_open),
        :order_options              => {},
        :limit                      => Gui.option(:default_page_size),
        :offset                     => 0
      })

       # Other keys you might see in locals for a CollectionContentController include:
         # :collection_size=>0,
         # :page_size=>8,
         # :filter=>{},
         # :unassociated_only=>false,
         # :selection=>false,
         # :object_view_name=>"Details",
         # :object_view_type=>"Organizer",
         # :disabled=>false,
         # :creation_disabled=>false,
         # :deletion_disabled=>false,
         # :removal_disabled=>false,
         # :addition_disabled=>false,
         # :traversal_disabled=>false,
         # :order=>{},
         # :collection_content=>true,
         # :limit=>8,
         # :offset=>0
    end

    attr_accessor :pretty_id
    
    def initialize(args = {})
      super
      locals[:filter] ||= locals[:simple_filter] || {}
      # Unassociated_only option overrides show_possible_assoc setting <--- Why would :unassociated_only ever happen?
      locals[:show_possible_assoc] = true if locals[:unassociated_only]
    end
    
    def columns
      widget.content.reject { |c| c.is_a?(Gui::Widgets::Button) || c.hidden? }
    end
    
    def button_widgets
      widget.content.select { |c| c.is_a?(Gui::Widgets::Button) && !c.hidden?(locals) }
    end
    
    def button_controllers
      bcs = button_widgets.map do |bw|
        # Yes, we're making Verticies on the fly because Collections are lazy_load by default.  This could go badly if a collection isn't set to lazy_load = true....maybe....
        v  = Gui::Vertex.new(widget:bw, parent_vertex:parent_vertex, obj:parent_obj)
        bc = Gui::WidgetControllers::CollectionButtonController.new(:vertex => v, :locals => locals, :controller_type => 'CollectionButton')
      end
    end
    
    def rendered_buttons
      button_controllers.map { |bc| bc.render }.compact.join("\n")
    end
        
    # @override
    def view_name
      widget.view_name
    end
    
    # FIXME move to init and get rid of this method
    def page_size
      local_size = self.locals[:page_size] || widget.page_size
    end
    
    # Starts at zero (can have a page with no items)
    def total_pages
      [(filtered_count / page_size.to_f).ceil, 1].max
    end

    def filtered_count
      vertex.get_filtered_count(locals[:show_possible_assoc])
    end
    
    def domain_obj
      # likely domain_obj is not Enumerable, so we'll wrap it in an array.  But
      # it is also likely that it is a 'new' domain object that hasn't been
      # created/saved to the database yet.  We only want to show domain objects
      # that exist in the database, so we filter that out.
      # - Filtering the non-existing is taking a very long time on large data sets...so we aren't doing it anymore
      Array(super)
    end

    def domain_obj_associations
      objects = has_join_class? ? vertex.domain_obj_associations : domain_obj
      # Because collections can represent an organizer, we have to wrap in an array sometimes
      objects = [objects].compact unless objects.is_a?(Array)
      objects = objects.map { |o| { :to => o } } unless has_join_class?
      objects
    end

    # FIXME || makes no sense here
    def unassociated_obj
      vertex.unassociated_obj.map { |o| {:to => o} } || []
    end
    
    def setup_collection(domain_obj)
      # called from gui_site/lib/gui_site/controller/controller.rb
      @domain_obj            = domain_obj
      @widget                = Gui::Widgets::ViewRef.new({}) # and here I thought we weren't coupled to gui_spec....sigh
      widget.view_name       = :Summary
      widget.data_classifier = @domain_obj.gb_most_generic_type
      widget.multiplicity    = 'many'
      widget.orderable       = false
      widget.expanded        = true
      @view                  = Gui.loaded_spec.retrieve_view(widget.view_name, widget.data_classifier, :Collection)
      widget.view            = view
    end
    
    # the name attribute of this widget's filter input element
    def filter_input_name(widget)
      f = widget.search_filter
      gttr = widget.association_class_widget? ? "#{widget.table_name}__#{widget.getter}" : widget.getter.to_s
      f.is_a?(Hash) ? gttr + '_' + f[:field].to_s : gttr
    end
    
    # column is a column or a string (used for 'type' column)
    def filter_option_selected(simple_filter, widget, option)
      filter_name = widget.is_a?(String) ? widget : filter_input_name(widget)
      val = simple_filter[filter_name] if simple_filter
      if val != option
        nil
      else
        'selected'
      end
    end
    
    # the value to fill in the widget's filter input element
    def filter_input_value(simple_filter, widget)
      (simple_filter && simple_filter[filter_input_name(widget)]) || widget.filter_value || ''
    end
    
    def search_all_filter_input_value(search_all_filter)
      search_all_filter && search_all_filter[:search_all_value] || ''
    end
    
    def filter_type(widget)
      f = widget.search_filter
      f.is_a?(Hash) ? f[:filter_type] : f
    end

    # TODO This isn't the place to do it but there needs to be a clear distinction to the user b/w nil values and not being authorized to read an attribute.
    # TODO why is it that only text widgets might have a decorator?
    # TODO display needs to format arrays of values (i.e. widget.multiple == true) much better than this    
    def display(widget, domain_obj)
      if widget.class == Gui::Widgets::Choice
        # TODO Enumerations are a bit weird because they behave like attributes but are backed by objects.  It is possible that a user may not be allowed to see certain enumeration literal values.  Think about this more...
        if widget.multiple
          return [] unless domain_obj
          enum_objs = authorized_attribute_read(domain_obj, widget.getter)
          enum_objs.map { |eo| eo&.value }.compact&.join(', ')
        else
          return nil unless domain_obj
          enum_obj = authorized_attribute_read(domain_obj, widget.getter)
          enum_obj&.value
        end
      else 
        value = authorized_attribute_read(domain_obj, widget.getter) if domain_obj
        value = nil if widget.multiple && value&.empty?
        case widget
        when Gui::Widgets::File
          value&.filename || ''
        when Gui::Widgets::RichText
          # This stinks.  I hate complex attributes...
          content  = value&.content
          language = value&.markup_language
          if language&.value&.to_s == 'HTML'
            value  = ::Nokogiri::HTML(value).text
          end
          content = widget.decorator.call(content) if widget.decorator
          return content.to_s if content.nil? || content.strip.empty?
          # Limit the return text display to the first 30 characters
          # FIXME replace this magic number with Gui.option setting
          content.length >= (Gui.option(:richtext_display_max).to_i + 4) ? (content[0..(Gui.option(:richtext_display_max).to_i - 1)] + ' ...') : content
        when Gui::Widgets::Float
          return '' if value.nil? || value.to_s.empty?
          Float(value).to_s
        when Gui::Widgets::Timestamp
          return '' if value.nil? || value.to_s.empty?
          case widget
          when Gui::Widgets::Date
            value.strftime("%Y-%m-%d")
          when Gui::Widgets::Time
            value.strftime("%l:%M:%S%P")
          else # Gui::Widgets::Timestamp
            value.strftime("%Y-%m-%d%l:%M:%S%P")
          end
        else
          value
        end
      end
    end
        
    # Returns the paginated objects wrapped into hashes
    # objects - the objects to be wrapped (in the form of a hash with keys :to and :through)
    # additions - pending_association_additions
    # removals - pending_association_removals
    # deletions - pending_association_deletions
    # unassociated - whether or not unassociated objects are being displayed
    def paginated_objects(objects, additions, removals, deletions, unassociated = false)
      pending_assocs = additions + removals
      objects.map do |o|
        o = {:to => o} unless o.is_a?(Hash)
        o[:associated?] = !unassociated
        o[:pending_assoc_change] = is_object_included_in_array?(pending_assocs, o[:to], o[:through])
        o[:pending_deletion] = is_object_included_in_array?(deletions, o[:to], o[:through])
        o
      end
    end
    
    # Number of columns to be displayed in the rendered table
    def number_of_columns
      return @noc if @noc
      n = 1 # filter_toggle_cell
      n += 1 if widget.orderable # position field
      n += 1 # 'Edit' link
      n += 1 if has_assoc_class? && !locals[:show_possible_assoc] # 'Edit' link for association class
      n += columns.count
      n += 1 if show_multiple_types
      @noc = n
    end
    
    # ----- Model helpers -----
    def create_constructors_list(classes)
      constructor_list = []
      classes.each do |c|
        c.constructors.each do |constructor, proc|
          constructor_list << [c, constructor.to_s]
        end
      end
      constructor_list
    end
    # -------------------------
    
    def domain_obj_row_class(is_associated, pending_association, pending_deletion, to_obj, show_unassociated)
      # All rows are unselected on page load
      class_names = 'unselected '
      # TODO There is no way to differentiate between adding assoc and breaking assoc here???  A broken assoc shouldn't have table-success class
      class_names << 'pending_assoc table-success ' if pending_association 
      class_names << 'pending_deletion table-danger ' if pending_deletion
      class_names << 'no_to_object ' if to_obj.nil?
      if is_associated
        class_names << 'domain_obj '
      else
       class_names << 'unassociated_object ' + (show_unassociated ? ' visible ' : ' ')
      end
    end

    # Determines if a given object (or it's association class) is in an array
    def is_object_included_in_array?(array_of_hashed_class_and_id, obj, through)
      return false unless obj || through
      array_of_hashed_class_and_id.any? do |id_class_hash|
        (obj && id_class_hash['classifier'] == obj.class.to_s && id_class_hash['identifier'] == obj.id.to_s) ||
        (through && id_class_hash['through_classifier'] == through.class.to_s && id_class_hash['through_identifier'] == through.id.to_s)
      end
    end
    
    def show_multiple_types
      @show_multiple_types ||= association_types(parent_obj, widget.getter).length > 1 && vertex.show_multiple_types
    end
    
    def concrete_types(obj, getter)
      association_types(obj, getter).reject(&:abstract?)
    end
    
    def association_types(obj, getter)
      obj && obj.respond_to?("#{getter}_type") && obj.send("#{getter}_type") || []
    end
    
    def parent_is_ad_hoc_page?
      parent_obj.class < Gui::AdHocPageObject
    end
  end
end
