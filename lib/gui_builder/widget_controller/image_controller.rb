require_relative 'file_controller'

module Gui::WidgetControllers
  class ImageController < FileController
    def relative_image_link
      relative_download_link
    end
    
    # Image specific helper. TODO: move to methods_to_sub_for_definitions when it works with arguments
    # Determine whether or not to render the img tag, in order to prevent requests to the server for images that don't exist
    def image_is_displayable?(parent_obj, getter)
      return false if parent_obj.new?
      assoc = parent_obj.class.properties[getter.to_sym]
      # The part after the colon is untested and it should probably be (getter.to_s + '_id').to_sym
      assoc[:derived] ? true : parent_obj[getter.to_sym]
    end
  end
end