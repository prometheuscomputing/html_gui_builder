require_relative 'abstract_view_controller'

module Gui::WidgetControllers
  class ContextController < AbstractViewController        
    def expanded?
      self.expanded || widget.expanded
    end
  end
end
