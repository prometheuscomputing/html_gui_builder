require_relative 'widget_controller'
require 'kramdown'
require 'redcloth'

module Gui::WidgetControllers
  class RichTextController < WidgetController
    def domain_content
      (domain_obj && domain_obj.respond_to?(:content) && domain_obj.content) || ''
    end
    
    def content_value
      previous_value['content'] || domain_content
    end

    def initial_content_value
      initial_value['content'] || domain_content
    end
    
    # Get the domain_object's language (with no defaults if not set)
    def domain_obj_language
      domain_obj.markup_language if domain_obj
    end
    
    def selected_language
      (domain_obj_language || widget.language || widget.default_language_string)
    end
    
    def language_value
      _language_value(previous_value['markup_language'] || selected_language)
    end
    
    def initial_language_value
      _language_value(initial_value['markup_language'] || selected_language)
    end
    
    def _language_value lv
      case
      when lv.is_a?(String)
        lv
      when lv.is_a?(Symbol)
        lv.to_s
      else # it should be an enumeration instance
        lv.value
      end
    end
    private :_language_value

    def fix_relative_markdown_links(pretty_value, parent_obj, getter)
      if parent_obj
        parent_classifier = parent_obj.class.to_s.gsub("::","/")
        parent_id = parent_obj.id
        # relative_matching_index = modified_value.index("](#{getter}/")
        absolute_url = "/#{parent_classifier}/#{parent_id}/#{getter}"
        pretty_value.gsub("](#{getter}/", "](#{absolute_url}/")
      else
        pretty_value
      end
    end  
    # # Fix for haml bug that eats 1 trailing newline.
    # # Works by inserting extra trailing newline if any trailing newlines
    # def fix_haml_textarea_bug(content)
    #   content + ("\n" if content[-1] == "\n").to_s
    # end
    # # Fix for html textarea bug (in chrome and firefox) that eats 1 leading newline
    # # Works by inserting extra leading newline
    # def fix_html_textarea_newline_bug(content)
    #   "\n" + content
    # end
    
    # Set a class based on the selected text markup language
    # This is used to load the appropriate MarkItUp editor
    # markdown, latex, and textile are currently supported
    # def textarea_class
    #   "text-#{language_value.downcase}"
    # end
  end
  TextController = RichTextController
end