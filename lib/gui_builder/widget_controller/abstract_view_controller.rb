require_relative 'widget_controller'

module Gui::WidgetControllers
  class AbstractViewController < WidgetController
    def self.default_locals
      super.merge({
        :ajax_request => false,
        :standalone   => false
      })
    end
    
    attr_accessor :expanded
    attr_accessor :view_type
    
    def initialize(args = {})
      super

      @view = widget&.view
      
      # FIXME This assumes that there are is no other alternative other than Collection.  Not extensible.... FIXME
      @view_type = view.is_a?(Gui::Widgets::Organizer) ? :Organizer : :Collection if view

      @expanded  = (!widget.nil? && widget.expanded || locals[:expanded])
    end
    
    def object_view_name
      return @object_view_name if @object_view_name
      @object_view_name = widget.object_view_name
    end
    
    def object_view_type
      return @object_view_type if @object_view_type
      @object_view_type = widget.object_view_type
    end
    
    def pending_association_additions
      vertex.pending_association_additions
    end

    def pending_association_removals
      vertex.pending_association_removals
    end

    def pending_association_deletions
      vertex.pending_association_deletions
    end

    def expanded?
      expanded || false
    end
    
    def use_accessibility
      return @use_accessibility if @use_accessibility
      @use_accessibility = defined?(Ramaze) ? Ramaze::Current.session[:user].use_accessibility? : false
    end
    
    def to_class_name
      return @to_class_name if @to_class_name
      @to_class_name = create_link_label(view.data_classifier)
    end
    
    def to_class
      return @to_class if @to_class
      @to_class = view.data_classifier
    end
      
    def view_name
      view && view.view_name
    end
    
    def object_view_name
      return @object_view_name if @object_view_name
      @object_view_name = widget.view.object_view_name
    end
    
    # ----- Link Helpers -----
    def url_hash(view_name, view_type, parent_obj = nil, getter = nil)
      url_hash = {}
      url_hash['view-name'] = view_name if view_name
      url_hash['view-type'] = view_type if view_type
      if parent_obj && !parent_obj.is_a?(Gui::AdHocPageObject)
        url_hash['assoc-classifier'] = parent_obj.class
        url_hash['assoc-id'] = parent_obj.id
        url_hash['assoc-getter'] = getter
      end
      url_hash
    end
    
    # @return [String] a URL string for the create page
    def create_link_href(klass, view_name, view_type, parent_obj, getter)
      Gui.create_url_from_hash(klass, 'new', url_hash(view_name, view_type, parent_obj, getter))
    end
    
    # def create_constructor_link_href(klass, constructor, view_name, view_type, parent_obj, getter)
    #   Gui.create_url_from_hash(klass, "new_#{constructor}", url_hash(view_name, view_type, parent_obj, getter))
    # end
    
    def create_link_label(klass)
      ret = klass.name.split('::').last
      ret = ret.split /(?=[A-Z])(?<=[a-z])|(?<=[A-Z])(?=[A-Z][a-z])/
      ret = ret.join(" ").gsub("_"," ")
      ret
    end
    
    # def create_constructor_link_label(constructor)
    #   constructor.split('_').map(&:capitalize).join(' ')
    # end
    
    # Adding in the assoc info is unnecessary and just results in bad behavior!
    def edit_link_href(row, view_name = nil, view_type = nil, parent_obj = nil, getter = nil)
      return view_link_href(row, view_name, view_type)
      # Gui.create_url_from_object(row, url_hash(view_name, view_type, parent_obj, getter))
    end
    
    def view_link_href(row, view_name = nil, view_type = nil)
      Gui.create_url_from_object(row, url_hash(view_name, view_type))
    end
    # ----------------------------
    
    # TODO for this method and #create_constructable_list, investigate using class.descendents (from activesupport) instead of class.children
    def _creatable_classes(parent_obj, getter, klass, through_klass)
      if through_klass
        creatable_classes = [through_klass] + through_klass.children
      else
        if parent_obj
          creatable_classes = parent_obj.send(getter.to_s + '_type')
        else
          creatable_classes =  [klass] + klass.children
        end
      end
      
      creatable_classes = creatable_classes.reject(&:abstract?)
      # FIXME policy here . this isn't quite right, we want to show only the classes the user can create.  we need a method that is more nuanced than this
      creatable_classes = creatable_classes.select { |klass| authorize_creation(klass) }
    end
    private :_creatable_classes
    
    def create_class_list(parent_obj, getter, klass, through_klass = nil)
      creatable_classes = _creatable_classes(parent_obj, getter, klass, through_klass)
      creatable_classes = creatable_classes.uniq.sort { |a, b| a.name <=> b.name }
    end
    
    def create_constructable_list(parent_obj, getter, klass, through_klass = nil)
      creatable_classes = _creatable_classes(parent_obj, getter, klass, through_klass)
      creatable_classes.map(&:constructors)
    end
    
    def create_constructor_link_href(constructor_key, constructor_values, view_name, view_type, parent_obj, getter)
      klass = constructor_values[:classifier]
      new_url_param = constructor_values[:method] == nil ? "new" : "new_#{constructor_key}"
      Gui.create_url_from_hash(klass, new_url_param, url_hash(view_name, view_type, parent_obj, getter))
    end
    
    def create_constructor_link_label(constructor_values)
      display_name = constructor_values[:name].to_s.demodulize
      display_name = display_name.split /(?=[A-Z])(?<=[a-z])|(?<=[A-Z])(?=[A-Z][a-z])/
      display_name = display_name.join(" ").gsub("_"," ")
      display_name
    end
    
    def position_getter(assoc_info)
      assoc_info[:opp_getter].to_s.singularize + '_position'
    end
    
    def position_of(domain_object, assoc_info)
      (domain_object[position_getter(assoc_info).to_sym] || 0) + 1
    end
    
    def domain_obj_to_s(*objs)
      # The following are what are needed to get the object that this row represents.
      # The id of the object that this row represents
      # The table_name for the object that this row represents
      objs.map { |obj| obj ? obj.class.name + "___" + obj.id.to_s : '' }.join('_____')
    end

    def domain_id_for(domain_obj)
      (domain_obj&.is_a?(ORM_Class)) ? (domain_obj.id || 'new') : nil
    end
  end
end