require_relative 'abstract_view_controller'

module Gui::WidgetControllers
  class OrganizerController < AbstractViewController
    def self.default_locals
      super.merge({
        :assoc_obj => false,
        :to_one    => false
      })
    end
    
    def initialize(args = {})
      super
      domain_obj # sets it if it isn't set
    end
    def close_on_save_and_leave?
      return true unless defined?(Ramaze) && Ramaze::Current.session && Ramaze::Current.session.flash[:current_breadcrumbs]
      previous_node = Ramaze::Current.session.flash[:current_breadcrumbs].previous_node
      if previous_node
        previous_node.domain_obj_class == 'Gui::Home'
      else
        true
      end 
    end
    
    def breadcrumbs
      if defined?(Ramaze) && Ramaze::Current.session && Ramaze::Current.session.flash[:current_breadcrumbs]
        Ramaze::Current.session.flash[:current_breadcrumbs].path.reverse
      else
        []
      end
    end
    
    def view_ref
      column_widgets = view.content
      view_name = if view
                    view.view_name
                  else
                    nil
                  end
      view_ref = nil
      column_widgets.each do |w|
        if w.kind_of? Gui::Widgets::ViewRef
          view_ref = w
          break
        end
      end
      view_ref || nil
    end

    # # returns true is the vertex passed has any children that are not ViewRef 
    # # widgets -- this means that they will be form widgets
    # def has_form_elements? vertex
    #   is_form = false
    #   vertex.children.each do |c|
    #     unless (c.widget.kind_of?(Gui::Widgets::ViewRef) || c.widget.kind_of?(Gui::Widgets::Link))
    #       is_form = true 
    #     end
    #     break if is_form
    #   end
    #   is_form
    # end

    # FIXME remove.  never used.
    # def show_create_form?
    #   is_composition? || Gui::Home.root_classes.include?(data_classifier)
    # end

    # def space
    #   (' ' * $indent).to_s
    # end
    # 
    # $indent = 0
    # 
    # # Returns true is there is any form elements under the vertex passed in,
    # # even if they are wrapped in another organizer,  or any of its parent
    # # vertices
    # def form? vertex
    #   $indent += 2
    # 
    #   is_form = false
    #   if vertex
    #     # check the current vertex for form elements
    #     is_form = (has_form_elements? vertex)
    #     unless is_form
    #       # if this is the root of the GUI do not recurse down
    #       if vertex.gui_root
    #         vertex.children.each do |c|
    #           if c.widget.kind_of?(Gui::Widgets::ViewRef)
    #             if c.view && c.view.kind_of?(Gui::Widgets::Organizer) 
    #               is_form = form? c
    #             end
    #           elsif c.widget.kind_of? Gui::Widgets::Link
    #             # TODO (josh, 07/06/10 17:49:57) - for some reason is_form was
    #             # set to true here, I'm not sure why, so I'm commenting it out
    #             # for now to see
    #             #is_form = true
    #           end
    # 
    #           break if is_form
    #         end
    #       end
    #     end
    #   end
    #   $indent -= 2
    #   is_form
    # end
  end
end