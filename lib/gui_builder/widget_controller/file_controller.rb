require_relative 'widget_controller'

module Gui::WidgetControllers
  class FileController < WidgetController
    # This uses the URL of the current page as a base
    def relative_download_link
      widget.getter
    end
    
    def relative_delete_link
      relative_download_link + '/delete'
    end
    
    def effective_last_update
      if locals[:continue_edit]
        initial_value['last_update']
      else
        last_update
      end
    end
    
    def last_update
      if o = domain_obj
        if bd = o.binary_data
          if last_update_time = (bd[:updated_at] || bd[:created_at])
            last_update_time.to_s_usec # column access because it is added by the timestamps plugin, which doesn't add the method.
          else
            ""
          end
        end
      end
    end
    
    def initial_filename
      o = domain_obj
      o.filename if o
    end
    
    def absolute_download_link(parent_obj, relative_download_link)
      '/' + Gui.route_for(parent_obj.class) + '/' + parent_obj.id.to_s + '/' + relative_download_link
    end
    
    def absolute_image_link(*args); absolute_download_link(*args); end
    
    def previous_filename(previous_value)
      if previous_value['filename'].is_a?(Hash)
        # Is a normal file upload
        previous_value['filename'][:filename]
      else
        # Is a filename from a previous edit.
        previous_value['previous_filename']
      end
    end
    
    def previous_mime_type(previous_value)
      if previous_value['filename'].is_a?(Hash)
        # Is a normal file upload
        previous_value['filename'][:type]
      else
        # Is a filename from a previous edit.
        previous_value['previous_mime_type']
      end
    end
   
  end
end