require_relative 'abstract_view_controller'

#  TODO make a Style widget that is separate from this.  Context should not carry style!
module Gui::WidgetControllers
  class StyleController < AbstractViewController
    
    def context_tag_and_css_style
      ["div", css_for(widget.style_options)]
    end

    def context_label_tag_and_css_style
      ["div", css_for(widget.style_options_label)]
    end
    
    def css_for(style)
      style.map { |k, v| "#{k}: #{v}; " }.join
    end
  end
end
