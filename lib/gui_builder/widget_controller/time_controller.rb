require_relative 'widget_controller'

module Gui::WidgetControllers
  class TimeController < WidgetController
    def format_value(value)
      return '' if value.nil? || (value.is_a?(String) && value.empty?)
      value = Time.parse(value) if value.is_a?(String)
      value.strftime("%l:%M:%S%P")
    end
  end
end
