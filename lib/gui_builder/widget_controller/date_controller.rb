require_relative 'widget_controller'

module Gui::WidgetControllers
  class DateController < WidgetController
    def format_value(value)
      return '' if value.nil? || (value.is_a?(String) && value.empty?)
      value = Time.parse(value) if value.is_a?(String)
      value.strftime("%B %-d, %Y")
    end
  end
end