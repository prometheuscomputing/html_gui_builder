module Gui_Builder_Profile
  class RichText
    def eql? other_richtext
      if other_richtext.nil? && self.nil?
        return true
      elsif other_richtext.nil? || self.nil?
        return false
      elsif !self.class.eql?(other_richtext.class)
        return false
      else
        return content_eql?(other_richtext) && markup_language_eql?(other_richtext)
      end
    end
    def content_eql? other_richtext
      c  = self.content
      oc = other_richtext.content
      c.eql?(oc) || (c == nil && oc == '') || (c == '' && oc == nil) || safe_string(c).eql?(safe_string(oc))
    end

    def markup_language_eql? other_richtext
      ml  = self.markup_language || Gui.loaded_spec.default_language
      oml = other_richtext.markup_language || Gui.loaded_spec.default_language
      ml.eql?(oml) || (ml == nil && oml == '') || (ml == '' && oml == nil)
    end
    
    # TODO these are copied from GuiDirector simple_widget.rb.  They are in different scopes but should be combine.
    # Trims off the leading and trailing newline
    # Necessary for Haml bug that eats 1 trailing newline if any,
    # and for HTML bug (in chrome and firefox) that eats 1 leading newline.
    # TODO: Make more efficient somehow. Can't use regex ^$ operators on multiline strings.
    # WARNING: this method will not do what you want it to if you call #remove_carriage_returns before this
    def trim_leading_and_trailing_newlines(value)
      return value unless value.is_a?(::String)
      value.sub(/\A(\r|\n)+/m, '').sub(/(\r|\n)+\z/m, '')
    end
    # Remove all carriage returns
    # Browsers insert carriage returns wherever newlines are in text areas.
    # Need to remove them all, even from the database (from data imports and for older database compatibility)
    def remove_carriage_returns(value)
      return value unless value.is_a?(::String)
      value.gsub(/\r/, '')
    end
    
    def safe_string value
      return value unless value.is_a?(::String)
      remove_carriage_returns(trim_leading_and_trailing_newlines(value.force_encoding('utf-8')))
    end  
  end
end