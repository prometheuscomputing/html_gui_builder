# TODO: replace method override system in favor of passing the desired URL structure to the rendering director
#
# NOTE: these methods can and should be overridden by the application project
#       They are overridden in gui_site's url_encoder.rb file
# These methods should be considered stubs used for testing only (so html_gui_builder may be
#  tested independently of gui_site).
module Gui
  def self.create_url_from_object(obj, args = {})
    url = ""
    if obj.nil? || obj.class < Gui::AdHocPageObject
      url = "/"
    else
      path = obj.class.name.split("::")
      klass = path.pop
      package = path.join("_")
      if package
        url += "/#{package.to_ramaze_url}" # Have to downcase b/c of the way Ramaze maps controllers
      end
      
      url += "/#{klass}/"
      id = (obj.id.nil? || (obj.id.is_a?(String) && obj.id.empty?)) ? 'new' : obj.id
      url += "#{id}/"
    end
    
    params = []
    reduce_url_options(args, obj.class)
    args.each do |key, value|
      params << "#{key}=#{value}"
    end
    
    param_string = params.join("&")
    
    unless param_string.empty?
      url = url + "?" + param_string
    end
    return url
  end
  
  def self.create_image_url(richtext, filename)
    "#{richtext}/images/#{filename}"
  end
  
  def self.create_url_from_breadcrumb(node)
    url = '/'
    unless node.domain_obj_class ==  Gui.loaded_spec.default_classifier.to_s
      url << "#{node.package.to_s.to_ramaze_url}/" if node.package && !node.package.empty? && node.package != 'Object'
      url << "#{node.domain_obj_class}/" 
      url << "#{node.domain_obj_id}/"
    end
    
    options = node.view_hash
    self.reduce_url_options(options, node.domain_obj_class)
    
    if options.any?
      url << '?'
      param_strings = []
      options.each do |param, value|
        param_strings << "#{param}=#{value}"
      end
      url << param_strings.join('&')
    end
    url
  end
  
  def self.create_history_url_from_breadcrumb(node)
    url = '/change_tracker/history/'
    url << node.package + '/'
    url << node.domain_obj_class + '/'
    url << node.domain_obj_id + '/'
  end
  
  def self.create_clone_url_from_breadcrumb(node)
    url = '/clone_popup/'
    url << node.package + '/'
    url << node.domain_obj_class + '/'
    url << node.domain_obj_id + '/'
  end
  
  def self.create_breadcrumb_from_url(url)
    base_url, separator, params_string = url.partition('?')
    base_url_parts = base_url.split('/').reject(&:empty?)
    # TODO: make better recognition system for url parts. This is not entirely accurate
    package = 'Object'
    domain_obj_class = Gui.loaded_spec.default_classifier.to_s
    domain_obj_id = 'new'
    case base_url_parts.length
    when 3
      package, domain_obj_class, domain_obj_id = base_url_parts
    when 2
      part_one, part_two = base_url_parts
      package = part_one
      domain_obj_class = part_two
    when 1
      domain_obj_class = base_url_parts[0]
    when 0
    else
      raise "Unknown number of url parts in URL: #{base_url_parts}"
    end
    params = {}
    if params_string
      params_string.split('&').each do |param_string|
        key, value = param_string.split('=')
        params[key] = value
      end
    end
    NavigationNode.new(package, domain_obj_class, domain_obj_id, params)
  end
    
  
  def self.create_url_from_hash(type, id, args = {})
    url = "/"
    unless type == Gui.loaded_spec.default_classifier.to_s
      url += "#{type.to_s.to_ramaze_url}/"
      url += "#{id}/" if id
    end
    
    
    params = []
    reduce_url_options(args, type)
    args.each do |key, value|
      params << "#{key}=#{value}"
    end
    
    param_string = params.join("&")
    
    unless param_string.empty?
      url = url + "?" + param_string
    end
    
    return url
  end

  private
  def self.reduce_url_options options, object_type
    options.delete('view-type') if options['view-type'] == Gui.loaded_spec.default_view_type
    options.delete('view-name') if options['view-name'] == Gui.loaded_spec.default_view_name
    options.delete('view-classifier') if options['view-classifier'].to_s == object_type.to_s
  end
end
