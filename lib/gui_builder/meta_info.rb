module HtmlGuiBuilder
  # Required
  GEM_NAME = "html_gui_builder"
  VERSION  = '8.10.0'
  AUTHORS  = ["Samuel Dana", "Josh McDade", "Julia Dana", "Ben Dana", "Michael Faughn"]
  SUMMARY = %q{Renders Gui Widgets into HTML}
  
  # Optional
  EMAILS      = ["s.dana@prometheuscomputing.com", "michael.faughn@nist.gov"]
  HOMEPAGE    = 'https://gitlab.com/prometheuscomputing/html_gui_builder'
  DESCRIPTION = %q{AJAX web application for viewing and editing data structured by a UML model.}
  
  LANGUAGE         = :ruby
  LANGUAGE_VERSION = ['>= 2.7']
  RUNTIME_VERSIONS = { :mri => ['>= 2.7'] }
  
  DEPENDENCIES_RUBY = {
    'lodepath'     => '~> 0.1',
    'haml'         => '~> 5.0',
    'haml-contrib' => '~> 1.0',
    'sass'         => '~> 3.7',
    'kramdown'     => '~> 2.3',
    'RedCloth'     => '~> 4.3',
    'nokogiri'     => '~> 1.11'
    }
  DEVELOPMENT_DEPENDENCIES_RUBY  = { }
end
