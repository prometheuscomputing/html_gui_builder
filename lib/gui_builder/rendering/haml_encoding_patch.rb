# Fix bad encodings from Markdown filter by forcing all encodings to be UTF-8
module Haml
  class Buffer
    class UTF8String < String
      def << text; super text.force_encoding(Encoding::UTF_8); end
    end
    alias original_initialize initialize
    def initialize(*args)
      original_initialize(*args)
      @buffer = UTF8String.new
    end
  end
end
