require 'gui_builder/widget_controller'
require 'haml'
require 'uri'

# Turn on Haml's ugly mode for efficiency and better whitespace handling
# Haml::Options.defaults[:ugly] = true
# Add span to list of 'preserved' tags.
# This causes Haml not to inject newlines into span tags
Haml::Options.defaults[:preserve] << 'span'

module Gui
  class HTMLBuilder
    
    def render_template(vertex, locals = {})
      type = vertex.widget.class.name.split('::').last
      get_widget_controller(vertex, type, locals.merge(:rendering_template => true)).render_template(locals) do
        vertex.children.map { |cv| render_template(cv, locals) }.join("\n")
      end
    end
    
    # Typical rendering options
    # locals = {
    #   :unassociated_only => true, 
    #   :expanded => true, 
    #   :hide_collection_header => true, 
    #   :selection => true,
    #   :selection_param_name => 'composer_option'}
    def render_vertex(vertex, locals = {})
      return '' unless vertex.renderable?
      type = vertex.widget.class.name.split('::').last
      # FIXME (locals used to be called options)....we're using options as a big pile of "options" that might be used while initialize a widget controller or for rendering or who knows what else. This is a code smell.  The "options" need to be differentiated so we know what values / parameters are going where and why.
      widget_controller = get_widget_controller(vertex, type, locals)
      # Render the widget and all child widgets
      widget_controller.render do
        vertex.children.map { |cv| render_vertex(cv, locals) }.join
      end
    end
    
    def get_widget_controller(vertex, type, locals)
      
      Gui.use_options([:collection_content, :rendering_template, :unassociated_only])
          # FIXME refactor
          vertex_type = get_vertex_type(vertex, type, locals[:collection_content], locals[:rendering_template], locals[:unassociated_only])
          controller_class = Gui.resolve_classifier("#{vertex_type}Controller", Gui::WidgetControllers)

          controller_type = vertex_type == 'CollectionFromOrganizer' ? 'Collection' : vertex_type

      args = {
        :controller_type  => controller_type,
        :vertex           => vertex,
        :display_type     => vertex.widget.display_type,
        :locals           => locals
      }
      controller_class.new(args)
    end
    
    def get_vertex_type(vertex, widget_type, collection_content, rendering_template, unassociated_only)
      #return 'SelectionContent' if vertex.gui_root && options[:selection]
      return 'CollectionContent' if vertex.gui_root && collection_content
      
      widget_type = vertex.widget.view.class.name.split('::').last if widget_type == 'ViewRef'
        
      is_composition = 
        vertex.parent_domain_obj && 
        vertex.widget &&
        vertex.widget.getter &&
        vertex.parent_domain_obj.class.respond_to?(:compositions) &&
        vertex.parent_domain_obj.class.compositions.any? { |c| c[:getter] == vertex.widget.getter.to_sym }
      
      # Unless rendering templates or composition, convert Organizer without a domain object to 'CollectionFromOrganizer'
      #   in order to give the user the ability to select an existing object (Organizers can't do that) in addition
      #   to the ability to create a new object.
      if widget_type == 'Organizer' && !is_composition && !rendering_template && (vertex.domain_obj.nil? || unassociated_only)
        widget_type = 'CollectionFromOrganizer'
      end
      widget_type
    end
  end
end
