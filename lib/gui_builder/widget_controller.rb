# Maintain this order of requires

# Require every ruby file in gui_builder/widget_controller/
require_dir(relative('widget_controller'))

# NOTE: The most abstract controller in widget_controller/ is _not_ widget_controller, it is base_controller.
#       This is because some of the listed controllers are not widget controllers.
