begin
  require 'rspec'
rescue LoadError
  require 'rubygems' unless ENV['NO_RUBYGEMS']
  gem 'rspec'
  require 'rspec'
end

$:.unshift(File.dirname(__FILE__) + '/../lib')
require 'html_gui_builder'

puts "Requiring gui_director"
begin
  # Look for gui_director as a sibling project to html_gui_builder
  local_gui_director_path = File.expand_path('../../../gui_director/lib', __FILE__)
  puts "Checking for gui_director at: #{local_gui_director_path}"
  $:.unshift local_gui_director_path
  require local_gui_director_path + '/gui_director'
  puts "USING LOCAL COPY OF gui_director".center(100, "-")
  puts "Found at #{local_gui_director_path}".center(100, "-")
rescue LoadError => e
  $:.shift # Remove local_gui_director_path from load path
  puts "Couldn't load local copy of gui_director"
  require 'gui_director'
end